#include <string.h>


bool show_display(int h, int m, int s, int return_line, int return_column) {

	bool pixel[10][12] = {{0}};
	bool number[5][3];

	bool zero[5][3] = 	{
					  	{1,1,1},
					  	{1,0,1},
					  	{1,0,1},
				   	  	{1,0,1},
					  	{1,1,1}
					  	};
	bool one[5][3] =  	{
						{0,0,1},
						{0,0,1},
						{0,0,1},
						{0,0,1},
						{0,0,1}
						};
	bool two[5][3] = 	{
						{1,1,1},
						{0,0,1},
						{1,1,1},
						{1,0,0},
						{1,1,1}
						};
	bool three[5][3] =  {
						{1,1,1},
						{0,0,1},
						{1,1,1},
						{0,0,1},
						{1,1,1}
						};
	bool four[5][3] = 	{
						{1,0,1},
						{1,0,1},
						{1,1,1},
						{0,0,1},
						{0,0,1}
						};
	bool five[5][3] = 	{
						{1,1,1},
						{1,0,0},
						{1,1,1},
						{0,0,1},
						{1,1,1}
						};
	bool six[5][3] = 	{
						{1,1,1},
						{1,0,0},
						{1,1,1},
						{1,0,1},
						{1,1,1}
						};
	bool seven[5][3] =  {
						{1,1,1},
						{0,0,1},
						{0,0,1},
						{0,0,1},
						{0,0,1}
						};
	bool eight[5][3] = 	{
						{1,1,1},
						{1,0,1},
						{1,1,1},
						{1,0,1},
						{1,1,1}
						};
	bool nine[5][3] = 	{
						{1,1,1},
						{1,0,1},
						{1,1,1},
						{0,0,1},
						{1,1,1}
						};

	int p_line;
	int p_column;
	int n_line;
	int n_column;

	//set first digit h-----------------------------------------------------------
	if(h >= 10) {
		memcpy(number, one, sizeof(one));
		h -= 10;
	} else {
		memcpy(number, zero, sizeof(zero));
	}

	for(p_line = 2, n_line = 0; p_line < 7 && n_line < 5; p_line++, n_line++) {
		for(p_column = 0; p_column < 3; p_column++) {
			pixel[p_line][p_column] = number[n_line][p_column];
		}
	} 
	//set last digit h
	switch(h) {
		case 0: memcpy(number, zero, sizeof(zero)); 	break;
		case 1: memcpy(number, one, sizeof(one)); 		break;
		case 2: memcpy(number, two, sizeof(two)); 		break;
		case 3: memcpy(number, three, sizeof(three));	break;
		case 4: memcpy(number, four, sizeof(four)); 	break;
		case 5: memcpy(number, five, sizeof(five)); 	break;
		case 6: memcpy(number, six, sizeof(six)); 		break;
		case 7: memcpy(number, seven, sizeof(seven)); 	break;
		case 8: memcpy(number, eight, sizeof(eight)); 	break;
		case 9: memcpy(number, nine, sizeof(nine)); 	break;
	}

	for(p_line = 2, n_line = 0; p_line < 7 && n_line < 5; p_line++, n_line++) {
		for(p_column = 3, n_column = 0; p_column < 6 && n_column < 3; p_column++, n_column++) {
			pixel[p_line][p_column] = number[n_line][n_column];
		}
	}

	//set first digit m--------------------------------------------------------------
	if(m >= 50) {
		memcpy(number, five, sizeof(five));
		m -= 50;
	} else if(m >= 40) {
		memcpy(number, four, sizeof(four));
		m -= 40;
	} else if(m >= 30) {
		memcpy(number, three, sizeof(three));
		m -= 30;
	} else if(m >= 20) {
		memcpy(number, two, sizeof(two));
		m -= 20;
	} else if(m >= 10) {
		memcpy(number, one, sizeof(one));
		m -= 10;
	} else {
		memcpy(number, zero, sizeof(zero));
	}

	for(p_line = 0, n_line = 0; p_line < 5 && n_line < 5; p_line++, n_line++) {
		for(p_column = 6, n_column = 0; p_column < 9 && n_column < 3; p_column++, n_column++) {
			pixel[p_line][p_column] = number[n_line][n_column];
		}
	} 
	//set last digit m
	switch(m) {
		case 0: memcpy(number, zero, sizeof(zero)); 	break;
		case 1: memcpy(number, one, sizeof(one)); 		break;
		case 2: memcpy(number, two, sizeof(two)); 		break;
		case 3: memcpy(number, three, sizeof(three)); 	break;
		case 4: memcpy(number, four, sizeof(four)); 	break;
		case 5: memcpy(number, five, sizeof(five)); 	break;
		case 6: memcpy(number, six, sizeof(six)); 		break;
		case 7: memcpy(number, seven, sizeof(seven)); 	break;
		case 8: memcpy(number, eight, sizeof(eight)); 	break;
		case 9: memcpy(number, nine, sizeof(nine)); 	break;
	}

	for(p_line = 0, n_line = 0; p_line < 5 && n_line < 5; p_line++, n_line++) {
		for(p_column = 9, n_column = 0; p_column < 12 && n_column < 3; p_column++, n_column++) {
			pixel[p_line][p_column] = number[n_line][n_column];
		}
	}
	
	//set first digit s----------------------------------------------------------------
	if(s >= 50) {
		memcpy(number, five, sizeof(five));
		s -= 50;
	} else if(s >= 40) {
		memcpy(number, four, sizeof(four));
		s -= 40;
	} else if(s >= 30) {
		memcpy(number, three, sizeof(three));
		s -= 30;
	} else if(s >= 20) {
		memcpy(number, two, sizeof(two));
		s -= 20;
	} else if(s >= 10) {
		memcpy(number, one, sizeof(one));
		s -= 10;
	} else {
		memcpy(number, zero, sizeof(zero));
	}

	for(p_line = 5, n_line = 0; p_line < 10 && n_line < 5; p_line++, n_line++) {
		for(p_column = 6, n_column = 0; p_column < 9 && n_column < 3; p_column++, n_column++) {
			pixel[p_line][p_column] = number[n_line][n_column];
		}
	} 
	//set last digit s
	switch(s) {
		case 0: memcpy(number, zero, sizeof(zero)); 	break;
		case 1: memcpy(number, one, sizeof(one)); 		break;
		case 2: memcpy(number, two, sizeof(two)); 		break;
		case 3: memcpy(number, three, sizeof(three)); 	break;
		case 4: memcpy(number, four, sizeof(four)); 	break;
		case 5: memcpy(number, five, sizeof(five)); 	break;
		case 6: memcpy(number, six, sizeof(six)); 		break;
		case 7: memcpy(number, seven, sizeof(seven)); 	break;
		case 8: memcpy(number, eight, sizeof(eight)); 	break;
		case 9: memcpy(number, nine, sizeof(nine)); 	break;
	}

	for(p_line = 5, n_line = 0; p_line < 10 && n_line < 5; p_line++, n_line++) {
		for(p_column = 9, n_column = 0; p_column < 12 && n_column < 3; p_column++, n_column++) {
			pixel[p_line][p_column] = number[n_line][n_column];
		}
	}


	return pixel[return_line][return_column];
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
